#include <iostream>
#include <opencv2/opencv.hpp>

int main()
{
	std::cout << "Hello OpenCV VideoCapture" << std::endl;
	cv::namedWindow("Hello", cv::WINDOW_NORMAL);

	cv::VideoWriter videoWriter; // .mp4 file writer

	cv::VideoCapture videoCapture(0); // capture from default camera
	// or:
	// cv::VideoCapture videoCapture(cv::CAP_DSHOW); // hint for camera capture technology to be used
	// cv::VideoCapture videoCapture("https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm"); // streaming source

	// optional:
	// videoCapture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	// videoCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
	while (1) {
		cv::Mat img;
		videoCapture >> img; // read image from capture device
		if (img.empty()) {
			break; // video finished
		}
		if (!videoWriter.isOpened()) {
			// first image captured: create videoWriter
			int w = (int)videoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
			int h = (int)videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);
			cv::Size size = cv::Size(w, h);
			double fps = videoCapture.get(cv::CAP_PROP_FPS);
			if (fps == 0) {
				fps = 25;
			}
			bool ok = videoWriter.open("hello.mp4", cv::VideoWriter::fourcc('a', 'v', 'c', '1'), fps, size);
			if (!ok) {
				std::cerr << "H.264 encoding support (openH264) may not be available" << std::endl;
			}
		}
		// optional: use a nice image filter here:
		cv::Mat canny_img;
		cv::Canny(img, canny_img, 100, 200); // Canny creates a gray level image
		cv::cvtColor(canny_img, canny_img, cv::COLOR_GRAY2BGR); // gray -> color

		videoWriter << canny_img; // send image to video output device
		cv::imshow("Hello", canny_img); // send image to OS/GUI system
		int c = cv::waitKey(10); // display image, wait for key stroke
		if (c == 'q') {
			break;
		}
	}
	return 0;
}
