### Pre-Requisites
 
- OpenCV libraries, either downloaded and installed, or successfully built from source
- compiler/linker toolchain like gcc or Visual Studio Communiy Edition
- cmake (highly recommended, not strictly needed if you like to write makefiles or edit .sln files)

### Build Instructions (Windows)

Clone this repository and create a build folder in this project root folder:
```
> mkdir build
> cd build
```
Let cmake know where your OpenCV root folder is and let it create the build files: 
```
> cmake .. -DOpenCV_DIR=D:\OpenCV4.5.2_downloaded_binaries\build
-- Selecting Windows SDK version 10.0.17763.0 to target Windows 10.0.19042.
-- OpenCV ARCH: x64
-- OpenCV RUNTIME: vc15
-- OpenCV STATIC: OFF
-- Found OpenCV: D:/OpenCV4.5.2_downloaded_binaries/build (found version "4.5.2")
-- Found OpenCV 4.5.2 in D:/OpenCV4.5.2_downloaded_binaries/build/x64/vc15/lib
-- You might need to add D:\OpenCV4.5.2_downloaded_binaries\build\x64\vc15\bin to your PATH to be able to run your applications.
-- Configuring done
-- Generating done
-- Build files have been written to: D:/Users/Frank/Desktop/hello_opencv_world/build
```
Note the remark about PATH in the output. 

Let cmake build the executable file from the source(s) by invoking your toolchain:

```
> cmake --build . --config Debug
Microsoft (R)-Build-Engine, Version 16.9.0+57a23d249 für .NET Framework
Copyright (C) Microsoft Corporation. Alle Rechte vorbehalten.

  Checking Build System
  Building Custom Rule D:/Users/Frank/Desktop/hello_opencv_world/CMakeLists.txt
  hello_opencv_world.cpp
  Hello_OpenCV_World.vcxproj -> D:\Users\Frank\Desktop\hello_opencv_world\build\Debug\Hello_OpenCV_World.exe
  Building Custom Rule D:/Users/Frank/Desktop/hello_opencv_world/CMakeLists.txt
```

Add OpenCV run-time libraries to the PATH:

```
> set PATH=%PATH%;D:\OpenCV4.5.2_downloaded_binaries\build\x64\vc15\bin
```

Run the executable:

```
> Debug\Hello_OpenCV_World.exe
```

You should see a black window with a yellow circle in it. Quit the program by pressing any key. You should also find in the current folder an image file containing the image displayed.

You may see some INFO lines on startup telling you about additional dynamic libraries loaded (or failed to load).

You may repeat to build and execute the Release configuration as well. 
